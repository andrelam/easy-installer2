/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models;

import ecorp.flash.lib.models.StepUI;
import java.util.List;

/**
 *
 * @author vincent Bourgmayer
 */
public class StepUi extends StepUI{
    
    final private String titleIconName;
    final private String instructionImgName;
    final private int averageTime; //in second. The average time required to finsh the step. It only concern 'load' type.
    /**
     * Constructor for StepUI object
     * @param type type of step ("action" if user action is needed or "load" if no user action is needed)
     * @param instructions instructions of the step
     * @param instructionImgName filename of the image to show with the instruction. shouldn't be null
     * @param title title of the current step
     * @param titleIconName icon to show beside to the title. Only for action type. Either provide null.
     * @param stepNumber the number of the step in the whole process
     * @param averageTime The number of second needed to finish the step (it's an average)
     */
    public StepUi(String type, List<String> instructions, String instructionImgName, String title, String titleIconName, String stepNumber, int averageTime){
        super(type, instructions, title, stepNumber);
        this.titleIconName = titleIconName;
        this.instructionImgName = instructionImgName;
        this.averageTime = averageTime;
    }
    
    /**
     * get the file name of the icon beside the title
     * @return return null if no value provided at constructor which is the case if type is "load" instead of "action"
     */
    public String getTitleIconName(){
        return this.titleIconName;
    }
    
    /**
     * get the file name of the main picture of the current step.
     * @return return null if no value provided at Constructor.
     */
    public String getInstructionImgName(){
        return this.instructionImgName;
    }
    
    /**
     * Get the average time required to finish the task
     * @return int the value or -1 if no value
     */
    public int getAverageTime(){
        return this.averageTime;
    }
}
