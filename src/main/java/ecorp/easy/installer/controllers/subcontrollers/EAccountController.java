/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;

import ecorp.easy.installer.controllers.MainWindowController;
import ecorp.easy.installer.threads.AskAccountTask;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author vincent
 * This is the controller for eAccount.fxml
 */
public class EAccountController extends AbstractSubController{
    private static final String MAIL_REGEX="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(MAIL_REGEX);

    @FXML VBox eAccountRoot;
    @FXML HBox alreadyAccountBox;
    @FXML HBox askAccountBox;
    @FXML Button checkMailBtn;
    @FXML TextField mailInput;
    @FXML Label addAccountInstruction;
    @FXML RadioButton radioButton;
    @FXML RadioButton radioButton2;
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources){
        super.initialize(location, resources);
    }
    
    @Override
    public void setParentController(MainWindowController parentController){
        super.setParentController(parentController);
        parentController.disableNextButton(true);
        askAccountBox.setOnMouseClicked((eh) -> {
            mailInput.requestFocus();
            radioButton.setSelected(true);
            parentController.disableNextButton(true);
            parentController.setNextButtonVisible(true);
        });
        mailInput.setOnMouseClicked((eh) -> {
            radioButton.setSelected(true);
            parentController.disableNextButton(true);
            parentController.setNextButtonVisible(true);
        });
        alreadyAccountBox.setOnMouseClicked( (eh) -> {
            if(mailInput.isFocused()) alreadyAccountBox.requestFocus();
            radioButton2.setSelected(true);
            parentController.disableNextButton(false);
            parentController.setNextButtonVisible(true);
        });
    }
    
    
    public void onCheckMailBtnClick(){
        System.out.println("Check Mail button clicked");
        final String userInput = mailInput.getText();
        parentController.disableNextButton(true);
        if(!radioButton.isSelected()) radioButton.setSelected(true);
        
        switch (checkMailFormat(userInput)) {
            case -1:
                addAccountInstruction.setText(i18n.getString("eAccount_lbl_invalidMail"));
                mailInput.requestFocus();
                break;
            case 0:
                ExecutorService executorSrv = Executors.newSingleThreadExecutor();
                AskAccountTask askInvitationTask = new AskAccountTask(userInput, i18n.getString("askAccount_string"));
                askInvitationTask.setOnSucceeded(workerStateEvent -> {
                    String result = (String)workerStateEvent.getSource().getValue();
                    setResultInstruction(result);
                    checkMailBtn.setText(i18n.getString("eAccount_btn_checkMail"));
                });
                executorSrv.submit(askInvitationTask);
                executorSrv.shutdown();
                checkMailBtn.setText(i18n.getString("eAccount_lbl_processing"));
                break;
            case 1:
                addAccountInstruction.setText(i18n.getString("eAccount_lbl_invalidMail"));
                mailInput.requestFocus();
                break;
            case 2:
                addAccountInstruction.setText(i18n.getString("eAccount_lbl_dontuseEmail"));
                mailInput.requestFocus();
                break;
            default:
                break;
        }
    }
    
    private void setResultInstruction(String result){
        //@TODO: if result become more complexe, perhaps the use of a JSON parsing lib will be required.
        //But as it is only " {errcode:value]. It is easier,simpler and lighter to handle it manually.
        String trimedResult = result.trim(); //remove empty line before and after
        String errCode = trimedResult.substring(trimedResult.lastIndexOf(":")+1, trimedResult.length()-1 ); //Get only the errcode
        int code = Integer.parseInt(errCode);
        
        String instruction;
        switch(code){
            case 100:
                instruction=i18n.getString("eAccount_lbl_invitationSent");
                parentController.disableNextButton(false);
                break;
            case 200:
                instruction=i18n.getString("eAccount_lbl_mailAlreadyUsed");
                break;
            case 300:
                instruction=i18n.getString("eAccount_lbl_unsupportedFormat");
                break;
            case 400:
                instruction=i18n.getString("eAccount_lbl_emailNotSent");
                break;
            case 500:
                instruction=i18n.getString("eAccount_lbl_onlineFailure");
                break;
            case 600:
                instruction=i18n.getString("eAccount_lbl_tryLater");
                break;
            default:
                instruction=i18n.getString("eAccount_lbl_contactSupport");
                break;
        }
        addAccountInstruction.setText(instruction);
    }
    
    private int checkMailFormat(String email){
        if (email == null || email.isEmpty()) {
                return -1;
        }
        if(! EMAIL_PATTERN.matcher(email).matches() ){
            return 1;
        }
        //Filter few cases not catched by previous check
        if(email.contains("--") || email.contains("!!") || email.contains("__") || email.contains("**") || email.contains("^^")){
            return 1;
        }
        //check user do not use e.email adress
        if(email.contains("@e.email")){
            return 2;
        }
        return 0;
    }
}
