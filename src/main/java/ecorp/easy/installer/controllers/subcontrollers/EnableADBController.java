/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


/**
 * FXML Controller class
 *
 * @author Vincent
 */
public class EnableADBController extends AbstractSubSteppedController  {

    @FXML private Label firstSubStep;
    @FXML private Label secondSubStep;
    @FXML private Label thirdSubStep;
    
    @FXML private ImageView enableADBImg;
    
    private ArrayList<Label> instructions; //Used to iterate over instructions
    private int currentEmphasedLbl =0;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources); //To change body of generated methods, choose Tools | Templates.
        this.instructions = new ArrayList<>();
        
        instructions.add(firstSubStep);
        instructions.add(secondSubStep);
        instructions.add(thirdSubStep);
    }
    
    private void updateImage(){
        System.out.println("currentStepId= "+currentSubStepId);
        Image image;
        try{
            image = new Image(this.parentController.getClass().getResourceAsStream("/images/enableADB"+(currentSubStepId+2)+".png"));
        }catch(Exception e){
            image = null; 
        }
        this.enableADBImg.setImage(image);
    }

    @Override
    protected void onNextButtonClicked() {
        //Instruction for enabling Developer mode

        deemphasizeLabel(instructions.get(currentEmphasedLbl));
        
        if(currentEmphasedLbl == instructions.size()-1)
            currentEmphasedLbl = 0;
        else{
            currentEmphasedLbl++;
        }
        
        emphasizeLabel(instructions.get(currentEmphasedLbl));
        updateImage();
        
        switch (currentSubStepId) {
            case 2:
                //Update UI to show instruction to enable ADB Debuging
                parentController.setViewTitle("debugADB_mTitle");
                firstSubStep.setText(i18n.getString("debugADB_instr_settings"));
                secondSubStep.setText(i18n.getString("debugADB_instr_search"));
                thirdSubStep.setText(i18n.getString("debugADB_instr_androidDebug"));
                break;
            case 5:
                parentController.setViewTitle("enableMTP_mTitle");
                firstSubStep.setText(i18n.getString("enableMTP_instr_settings"));
                secondSubStep.setText(i18n.getString("enableMTP_instr_scrollToUSBConfig"));
                thirdSubStep.setText(i18n.getString("enableMTP_instr_selectMTP"));
                break;
            case 7:
                parentController.resetNextButtonEventHandler();
                break;
            default:
                break;
        }
    }
}
