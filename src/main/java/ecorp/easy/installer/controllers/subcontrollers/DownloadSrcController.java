/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;


import ecorp.easy.installer.controllers.MainWindowController;
import ecorp.easy.installer.threads.DownloadTask;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.concurrent.Worker.State;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

/**
 * FXML Controller class
 *
 * @author Vincent Bourgmayer
 */
public class DownloadSrcController extends AbstractSubController {

    @FXML ProgressBar preparationProgressBar;
    @FXML Label progressLabel;
    @FXML Label progressTitle;
    @FXML Button restartDownloadBtn;
    
    private Map<String, String> sourcesToDownload;
    private Iterator<Map.Entry<String, String>> taskIterator;
    private DownloadTask currentTask;
    
    @Override    
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        restartDownloadBtn.setManaged(false);
        restartDownloadBtn.setVisible(false);
        //preparationProgressBar.setProgress(1.0f);
    }

    @Override
    public void setParentController(MainWindowController parentController){
        super.setParentController(parentController);
        sourcesToDownload = parentController.getThreadFactory().getSourcesToDownload();
        taskIterator =  sourcesToDownload.entrySet().iterator();
        startNextDownload();
        //startPreparationThread(parentController.getThreadFactory());
    }
    
    
    private boolean startNextDownload(){
        System.out.println("taskIterator has next ? "+taskIterator.hasNext());
        if(taskIterator.hasNext()){
            Map.Entry<String, String> sources = taskIterator.next();
            currentTask = new DownloadTask(sources.getKey(), sources.getValue(), i18n);
            
            
            currentTask.setOnSucceeded((WorkerStateEvent eh)->{
                progressLabel.textProperty().unbind();
                progressTitle.textProperty().unbind();
                preparationProgressBar.progressProperty().unbind();
                if((Boolean) eh.getSource().getValue() ){ //if Succeed
                    //if no more download to do, then preparation is over
                    if(! startNextDownload() ) this.onPreparationEnd();
                }else{ //if failure
                    //progressLabel.setText("Error during Download");
                    //Show a try again button ?
                    //progressLabel.setText(i18n.getString("preparationError"));
                    this.preparationProgressBar.getStyleClass().add("errorBar");
                    restartDownloadBtn.setManaged(true);
                    restartDownloadBtn.setVisible(true);
                }
            });
            
            currentTask.setOnFailed((WorkerStateEvent eh) -> {
                progressLabel.textProperty().unbind();
                progressTitle.textProperty().unbind();
                preparationProgressBar.progressProperty().unbind();
                this.preparationProgressBar.getStyleClass().add("errorBar");
                System.out.println(eh.getSource().getException().toString());
                this.progressLabel.setText(i18n.getString("download_lbl_downloadError"));
                restartDownloadBtn.setManaged(true);
                restartDownloadBtn.setVisible(true);
            });
            
            this.progressTitle.textProperty().bind(currentTask.titleProperty());
            this.preparationProgressBar.progressProperty().bind(currentTask.progressProperty());
            this.progressLabel.textProperty().bind(currentTask.messageProperty());
            this.preparationProgressBar.getStyleClass().remove("errorBar");
            new Thread(currentTask).start();
            return true;
        }
        return false;
    }
    
    //@Override
    public void onPreparationEnd() {
        preparationProgressBar.setProgress(1.0);
        progressLabel.setText(i18n.getString("download_lbl_complete"));
        progressTitle.setVisible(false);
        parentController.disableNextButton(false);
        parentController.setThread(null);
    }
    
    
    public void onTryAgainBtnClick(){
        
        if(Arrays.asList(State.RUNNING, State.SCHEDULED, State.READY).contains( currentTask.getState() ))
            currentTask.cancel();
        
        taskIterator = sourcesToDownload.entrySet().iterator(); 
        startNextDownload();
        
        this.preparationProgressBar.getStyleClass().remove("errorBar");
        restartDownloadBtn.setManaged(false);
        restartDownloadBtn.setVisible(false); 
    }
}
