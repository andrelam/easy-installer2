/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.controllers.MainWindowController;
import ecorp.easy.installer.models.Device;
import ecorp.easy.installer.threads.UploadToEcloudTask;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;


/**
 * FXML Controller class
 *
 * @author Vincent Bourgmayer
 */
public class FeedbackController extends AbstractSubController {
    
    private @FXML Button sendAnswerBtn;
    private @FXML TextArea commentArea;
    private @FXML Label thanksLabel;
    
    private Button selectedFeelings;
    
    private final static String SELECTED_STYLE="selected-feelings";
    String deviceModel = "undefined";
    
    @Override
    public void setParentController(MainWindowController parentController) {
        super.setParentController(parentController); //To change body of generated methods, choose Tools | Templates.
        parentController.setNextButtonText("feedback_btn_leave");
        Device device = parentController.getDevice();
        if(device != null){
            System.out.println("Device not null!");
            deviceModel = device.getModel()+", "+device.getDevice();
        }else
            System.out.println("device is null");
    }
    
    
    /**
     * What happened when user click on a feelings button
     * @param event 
     */
    public void onFeelingsSelected(MouseEvent event){
        //When clicking a the already selected feelings, it deselect this event
        Button clickedButton = (Button) event.getSource();
        if(clickedButton.equals(selectedFeelings))  {
            
            System.out.println("Deselect: "+selectedFeelings.getId());
            selectedFeelings.getStyleClass().remove(SELECTED_STYLE);
            selectedFeelings = null;
        }
        else{
            //If another button was selected before, unselect it
            if(selectedFeelings != null) selectedFeelings.getStyleClass().remove(SELECTED_STYLE);
            
            clickedButton.getStyleClass().add(SELECTED_STYLE);
            selectedFeelings =  clickedButton;
            System.out.println("Select:" +selectedFeelings.getId());
        }
    }
    
    /**
     * Send the feedback to ecloud
     * @param event 
     */
    public void onSendBtnClicked(MouseEvent event){
        String filePath = createAnswerFile();
        sendAnswerBtn.setDisable(true);
        UploadToEcloudTask uploadTask = new UploadToEcloudTask("https://ecloud.global/s/QLwyiZ4fysodiz3", filePath, "UUx3eWlaNGZ5c29kaXozOg==");
        
        uploadTask.setOnSucceeded(eh -> {
            if( (Boolean) eh.getSource().getValue() ){ //if success
                sendAnswerBtn.setManaged(false);
                sendAnswerBtn.setVisible(false);
                thanksLabel.setVisible(true);
                thanksLabel.setManaged(true);
                System.out.println("sending feedback: success");
            }
            else{ //if failure
                sendAnswerBtn.setText(i18n.getString("feedback_btn_sendTryAgain"));
                sendAnswerBtn.setDisable(false);
                System.out.println("sending feedback: failed");
            }
        });
                
        uploadTask.setOnFailed(eh->{
            sendAnswerBtn.setDisable(false);
            sendAnswerBtn.setText(i18n.getString("feedback_btn_sendTryAgain"));
            System.out.println("sending feedback error: "+eh.getSource().getException().toString());
        });
        new Thread(uploadTask).start();
        
    }
    
    /**
     * Create the file with feedback inside
     * @return 
     */
    private String createAnswerFile(){
        String answerFilePath = AppConstants.getWritableFolder()+"feedback-"+UUID.randomUUID()+".txt";

        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(answerFilePath, true)))) {
            out.write("Timestamp: "+System.currentTimeMillis()+"\n");
            out.write("Device model: "+this.deviceModel);
            if(selectedFeelings != null){
                out.write("\nFeelings: "+selectedFeelings.getId());
            }
            out.write("\nComment: \n"+this.commentArea.getText());
            
        } catch (IOException e) {
            System.err.println(e);
         return null;
        }
        return answerFilePath;
    }
}
