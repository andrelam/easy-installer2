/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;


import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.models.Device;
import ecorp.easy.installer.threads.DeviceDetectionTask;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
/**
 *
 * @author Vincent Bourgmayer
 */
public class DeviceDetectedController extends AbstractSubController{


    @FXML private Button buyNewDevice;
    @FXML private Button startDetectionButton;
    @FXML private Label detectionMsg;
    private int detectionAskCounter = 0;

    
    @Override
    public void initialize(URL location, ResourceBundle resources){
        super.initialize(location, resources);

        startDetectionButton.setManaged(false);
        startDetectionButton.setVisible(false);
        buyNewDevice.setVisible(false);
        buyNewDevice.setManaged(false);
        detectionMsg.setText(i18n.getString("detect_lbl_detecting"));
        startDetection();
    }        
    
    /**
     * Start the process of detection
     */
    public void startDetection(){
        startDetectionButton.setManaged(false);
        startDetectionButton.setVisible(false);
        buyNewDevice.setVisible(false);
        buyNewDevice.setManaged(false);
        ++detectionAskCounter;

        String labelMsg = i18n.getString("detect_lbl_detecting");

        detectionMsg.setText(labelMsg);
        DeviceDetectionTask detectorTask = new DeviceDetectionTask();
        detectorTask.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, (WorkerStateEvent t) -> {
            Device result = detectorTask.getValue();
            if (result == null ){ 
                startDetection();
                return; 
            }
            String model = result.getModel();
            if(model == null || model.isEmpty()) displayUnknowDeviceFound();
            else{
                //check that there is config file for this device
                URL resourceUrl = getClass().getResource("/yaml/"+result.getDevice()+".yml");
                if(resourceUrl == null){
                    displayIncompatibleDeviceFound(result.getModel());
                }else{
                    //AppConstants.setDeviceModel(result.getDevice());
                    detectionMsg.setText(String.format(i18n.getString("detect_lbl_compatibleDeviceFound"), model));
                    if(parentController != null){
                        parentController.setDevice(result);
                        parentController.disableNextButton(false);
                    }
                }
            }
        });
        try{
            new Thread(detectorTask).start();
        }
        catch(Exception e){
            e.printStackTrace();
            //@TODO: handle  Too many device Found Exception
        }
    }
    
    /**
     * Update UI to indicate that an unknow Device is found
     */
    private void displayUnknowDeviceFound(){
        detectionMsg.setText(i18n.getString("detect_lbl_unknownDeviceFound"));
        showTryAgainButton();
        displayBuyNewDeviceBtn();
    }
    
    /**
     * update UI to tell that an incompatible device has been found
     * @param model 
     */
    private void displayIncompatibleDeviceFound(String model){
        detectionMsg.setText(String.format(i18n.getString("detect_lbl_incompatibleDeviceFound"), model));
        showTryAgainButton();
        displayBuyNewDeviceBtn();
    }
    
    /**
     * Show a button to lead the user to the shop
     */
    private void displayBuyNewDeviceBtn(){
        buyNewDevice.setVisible(true);
        buyNewDevice.setManaged(true);
    }
    
    /**
     * Show a button to le the user retry the detection
     */
    private void showTryAgainButton(){
        startDetectionButton.setManaged(true);
        startDetectionButton.setVisible(true);
        startDetectionButton.setText(i18n.getString("detect_btn_tryWithAnotherDevice"));
    }
    
    /**
     * Load nextUI from parentController
     */
    private synchronized void loadNextUI(){
        parentController.loadSubScene();
    }

    /**
     * Open /e/ Shop in a browser
     */
    public void goToShop(){
        final String osName = AppConstants.OsName.toLowerCase();
        Runtime rt = Runtime.getRuntime();
        String url = "https://e.foundation/smartphones-avec-e-pre-installe/";
        try{
            if(osName.contains("win")){
                rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
            }else if(osName.contains("mac")){
                rt.exec("open " + url);
            }else if(osName.contains("nix") || osName.contains("nux")){
                //https://doc.ubuntu-fr.org/xdg-open
                rt.exec("xdg-open "+url);
            }
            else{
                //Not Supported OS
                throw new Exception("OS not supported");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
