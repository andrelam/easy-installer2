/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer;
import ecorp.flash.lib.utils.Constants;
import java.nio.file.FileSystems;
import java.io.File;

/**
 *
 * @author Vincent Bourgmayer
 * @author Omer Akram
 */
public abstract class AppConstants extends Constants{
    
    public final static String Separator = FileSystems.getDefault().getSeparator();
    public final static String OsName = System.getProperty("os.name");
    public final static String JavaHome = System.getProperty("java.home");
    public final static String SourcesFolderName = "sources";
    public final static String ScriptsFolderName = "scripts";
    public final static String AdbFolderName = "adb";
    public final static String ASK_ACCOUNT_KEY="askAccount";
    
    /**
     * Get path to the folder where the app download source to flash the device
     * @return 
     */
    public static String getSourcesFolderPath(){

        String path = getWritableFolder()+SourcesFolderName+Separator;
        File file = new File(path);
        if (!file.exists()) {
            file.mkdir();
        }
        return path;
    }
    
    /**
     * Get path to the folder where scripts are stored
     * @return 
     */
    public static String getScriptsFolderPath(){
        return getRootPath()+ScriptsFolderName+Separator;
    }
    
    /**
     * Get path to the folder which contain adb standalone
     * @return 
     */
    public static String getADBFolderPath(){
        return getRootPath()+AdbFolderName+Separator;
    }
    
    /**
     * Get the root path of the running app.
     * Based on "JavaHome/bin/"
     * @return 
     */
    public static String getRootPath(){
        return JavaHome+Separator+"bin"+Separator;
    }

    /**
     * Indicate if the app run from a snap
     * [Linux only]
     * @return 
     */
    private static boolean isSnap(){
        // The name "easy-installer" must match the name of the snap inside
        // snapcraft.yaml.
        return System.getenv("SNAP_NAME") != null && System.getenv("SNAP_NAME").equals("easy-installer");
    }
    
     /**
     * Return the path to a foldre that the app can write in
     * @return 
     */
    public static String getWritableFolder(){
        // Snap packages are read-only by nature, hence we can't
        // really write to "java.home" directory. This codepath
        // detects if we are running from a snap runtime and
        // returns a path that is specific to snap and is writable.
        if (OsName.equals("Linux") && isSnap()){
            return  System.getenv("SNAP_USER_COMMON")+Separator;
        }else{
            return getRootPath();
        }
    }
}
