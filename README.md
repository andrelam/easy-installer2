# Easy Installer

Members:
- Gaël
- Romain
- Alexis
- Vincent
- Arnau
- Manoj
- Omer

developer: [Vincent Bourgmayer](vincent.bourgmayer@e.email)
## This is a Readme

## Documentation
- [How to build from source with Gradle](../../wikis/Build-with-Gradle)
- [How to support new Device](../../wikis/Support-new-device)
- [How translation works](../../wikis/update-translation)

## Techno dependancy
- Java
- FXML < XML
- Groovy ? 
- YAML
- CSS

## Dependancy:
- Java 11+
- JavaFX 13+
- Flash-lib
- Gradle 4.10+
    - org.openjfx.javafxplugin
    - https://badass-jlink-plugin.beryx.org/
- SnakeYaml 1.24+ 
- Git
- Gitlab

